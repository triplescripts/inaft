import { StackUtils } from "./StackUtils.src";
import { SimpleScanner } from "./SimpleScanner.src";

export
class BrowserUI {
  constructor(system, container, longURI) {
    const doc = container.ownerDocument;

    this._system = system;
    this._longURI = longURI;
    this._shortURI = BrowserUI.getPseudoPathShortLabel(doc.location.pathname);

    BrowserUI.applySkin(doc.head);
    BrowserUI.injectUIElements(container);

    this._results = new Map();
    this._uiState = new BrowserUI.UIState();

    let $getItem = (selector) => doc.querySelectorAll(selector)[0];

    this._overviewListBox = $getItem("#test-list");
    this._overviewFailedLabel = $getItem("#overview-pane .failed.label");
    this._overviewPassedLabel = $getItem("#overview-pane .passed.label");
    this._overviewFailedSegment = $getItem("#overview-pane .failed.segment");
    this._overviewEntriesBox = $getItem("#overview-pane .contents");
    this._overviewLoadButton = $getItem("#file-source-chooser");

    this._detailsTitleBox = $getItem("#details-pane .title");
    this._detailsFailedLabel = $getItem("#details-pane .failed.label");
    this._detailsPassedLabel = $getItem("#details-pane .passed.label");
    this._detailsFailedSegment = $getItem("#details-pane .failed.segment");
    this._detailsEntriesBox = $getItem("#details-pane .contents");

    this._overviewLoadButton.onclick = () => void(this._system.getFiles());
    this._overviewLoadButton.focus();

    this._overviewListBox.addEventListener("click", this);

    this._outer = doc.defaultView.parent;
  }

  static getPseudoPathShortLabel(originalPath) {
    let index = originalPath.lastIndexOf("/");
    if (originalPath.endsWith("/")) {
      index = originalPath.lastIndexOf("/", index - 1);
    }
    return originalPath.substr(index + 1) + BrowserUI.CONTEXT_PSEUDOPATH;
  }

  _updateUI(delta) {
    const $ = JSON.stringify;
    let oldState = this._uiState;
    this._uiState = new BrowserUI.UIState(oldState, delta);
    let properties = Object.keys(this._uiState);
    for (let i = 0, n = properties.length; i < n; ++i) {
      let name = properties[i];
      if ($(this._uiState[name]) != $(oldState[name])) {
        switch (name) {
          case "configProblem":
            this._alertBadConfig();
          break;
          case "newResults":
          case "testPaths":
            this._populateOverview();
          break;
          case "visibleDetails":
            this._populateDetails();
          break;
        }
      }
    }
  }

  _populateOverview() {
    this._overviewEntriesBox.classList.remove("beckoning");

    const $ = BrowserUI.createMarkup;
    const $escape = BrowserUI.escapeAsHTML;
    const $attr = (type, path) => BrowserUI.concat(
      'class="' + type + ' test-set"',
      'data-test-path="' + $escape(path) + '"'
    );

    // NB: this may end up null here, due to a hack to give us a small bump
    // for UI responsiveness.
    const { testPaths } = this._uiState;
    let total = testPaths && testPaths.length;
    if (total == null) return;

    let passCount = 0;
    let failCount = 0;
    let markup = new BrowserUI.TextSplice();
    for (let i = 0, n = total; i < n; ++i) {
      let type = "";
      let results = this._results.get(testPaths[i]);
      if (results && !results.skipped) {
        if (results.passed && !results.metafailures.length) {
          type = "passed";
          ++passCount;
        } else {
          type = "failed";
          ++failCount;
        }
      }
      if (!results || !results.skipped) {
        markup.append(
          $('li', $attr(type, testPaths[i]), [
            $('span', 'class="label"', [
              $escape(BrowserUI.getShortNameFromTestPath(testPaths[i]))
            ])
          ])
        );
      }
      if (results && results.skipped) {
        --total;
      }
    }

    if (passCount + failCount < total) {
      this._overviewPassedLabel.classList.add("absent");
      this._overviewFailedLabel.classList.add("absent");
      this._changeFailBar(this._overviewFailedSegment);
    } else {
      this._overviewPassedLabel.textContent = passCount + " passed";
      this._overviewPassedLabel.classList.remove("absent");
      this._overviewFailedLabel.textContent = failCount + " failed";
      this._overviewFailedLabel.classList.toggle("absent", !failCount);
      this._changeFailBar(this._overviewFailedSegment, failCount / total);
    }

    this._overviewListBox.innerHTML = markup;
  }

  static getShortNameFromTestPath(path) {
    return BrowserUI.getTestPathParts(path)[1];
  }

  static getTestPathParts(path, end = undefined) {
    const start = "tests/".length;
    if (end == undefined) end = path.indexOf("/", start);
    let shortname = path.substring(start, end)
    return [
      path.substring(0, start),
      shortname,
      path.substring(start + shortname.length)
    ];
  }

  _changeFailBar(segment, ratio) {
    if (ratio == undefined) {
      segment.style.width = 0;
      segment.parentNode.classList.add("pending");
    } else {
      segment.style.width = (ratio * 100) + "%";
      segment.parentNode.classList.remove("pending");
    }
  }

  _populateDetails() {
    const path = this._uiState.visibleDetails;
    const results = this._results.get(path);

    this._detailsTitleBox.innerHTML = this._createPathMarkup(path);

    let content = new BrowserUI.TextSplice();
    if (results) {
      for (let i = 0, n = results.metafailures.length; i < n; ++i) {
        let metafailure = results.metafailures[i];
        content.append(this._createDetailItemMarkup(path, metafailure));
      }

      const details = results.details.slice().sort((a, b) => (a.id - b.id));
      const total = details.length;

      let failCount = 0;
      for (let i = 0; i < total; ++i) {
        const { id, failure, label } = details[i];
        content.append(this._createDetailItemMarkup(path, failure, id, label));
        if (failure) ++failCount;
      }

      if (results.metafailures.length > 0) {
        this._changeFailBar(this._detailsFailedSegment, 1);
        this._detailsFailedLabel.textContent = "failed";
        this._detailsFailedLabel.classList.remove("absent");
        this._detailsPassedLabel.classList.add("absent");
      } else {
        this._detailsFailedLabel.textContent = failCount + " failed";
        this._detailsFailedLabel.classList.toggle("absent", !failCount);
        if (!total) {
          this._changeFailBar(this._detailsFailedSegment, 0);
          this._detailsPassedLabel.textContent = "passed";
        } else {
          let passCount = total - failCount;
          this._changeFailBar(this._detailsFailedSegment, failCount / total);
          this._detailsPassedLabel.textContent = passCount + " passed";
        }
        this._detailsPassedLabel.classList.remove("absent");
      }
    } else {
      this._detailsFailedLabel.classList.add("absent");
      this._detailsPassedLabel.classList.add("absent");
      this._changeFailBar(this._detailsFailedSegment);
    }

    this._detailsEntriesBox.innerHTML = content;
  }

  _createPathMarkup(path, end = undefined) {
    const $ = BrowserUI.createMarkup;
    let parts = BrowserUI.getTestPathParts(path, end);
    parts[1] = $('span', 'class="shortname"', [
      BrowserUI.escapeAsHTML(parts[1])
    ]);
    return String(new BrowserUI.TextSplice(parts));
  }

  _createDetailItemMarkup(path, failure, id = 0, label = null) {
    const $ = BrowserUI.createMarkup;
    const $escape = BrowserUI.escapeAsHTML;
    if (failure) {
      let more = failure.stack || failure.extra;
      // Don't expose too much of our implementation details (i.e., avoid
      // printing the worker data URI into some user-visible part of the UI).
      // Also, the data URI is just long and makes the stack hard to read.
      more = StackUtils.replace(more, this._longURI, this._shortURI);
      var errorHTML = $('div', 'class="error-info"', [
        $escape(failure.message + "\n\n" + more)
      ]);
      var type = "failed";
    } else {
      var type = "passed";
    }

    if (id > 0) {
      if (!label) label = $escape(path) + " test #" + $escape(id);
      var topbarHTML = $('div', 'class="item-info"', [
        $('span', 'class="sigil"'),
        $('span', 'class="label"', [ label ])
      ]);
    }

    return $('div', 'class="' + type + ' test-detail"', [
      topbarHTML || "",
      errorHTML || ""
    ]);
  }

  _alertBadConfig() {
    this._changeFailBar(this._overviewFailedSegment, 1);
    this._overviewEntriesBox.classList.remove("beckoning");

    const path = "tests/config.json"; // XXX

    this._detailsTitleBox.innerHTML = this._createPathMarkup(path, Infinity);
    this._changeFailBar(this._detailsFailedSegment, 1);

    this._detailsEntriesBox.innerHTML = this._createDetailItemMarkup(path, {
      message: this._uiState.configProblem.outer,
      extra: this._uiState.configProblem.inner
    });
  }

  handleEvent(event) {
    switch (event.type) {
      case "click": return void(this._onClick(event));
      default: throw new Error("Unexpected event: " + event.type);
    }
  }

  _onClick(event) {
    this._selectTestSetFromClickedBox(event.target);
  }

  _selectTestSetFromClickedBox(node) {
    let target = this._getHitTestSetItem(node);
    let items = this._overviewListBox.querySelectorAll("li.test-set");
    for (let i = 0, n = items.length; i < n; ++i) {
      items[i].classList.toggle("selected", items[i] == target);
    }
    this._updateUI({
      visibleDetails: target.dataset.testPath
    });
  }

  _getHitTestSetItem(node) {
    while (node) {
      if (node.parentNode == this._overviewListBox &&
          node.classList.contains("test-set")) {
        return node;
      }
      node = node.parentNode;
    }
    return null;
  }

  acknowledgeTests(paths) {
    this._updateUI({ testPaths: paths });
  }

  showConfigProblem(error) {
    const outer = error.message;
    const inner = error.innerException.message;
    this._updateUI({ configProblem: { outer: outer, inner: inner } });
  }

  markTestAsSkipped(path) {
    this._results.set(path, { skipped: true });
    // Force an update (use new array slice to thwart identity comparison)
    this._updateUI({ testPaths: this._uiState.testPaths.slice() });
  }

  showResults(path, results) {
    this._results.set(path, results);
    this._updateUI({ newResults: path });
  }

  static createChildElement(parent, childName) {
    return parent.appendChild(parent.ownerDocument.createElement(childName));
  }

  static createMarkup(name, attributes = "", content = []) {
    return (
      "<" + name + " " + attributes + ">" +
      content.join("") +
      "<" + "/" + name + ">"
    );
  }

  static concat() {
    return Array.from(arguments).join("\n");
  };

  static escapeAsHTML(content) {
    let result = "";
    let scanner = new SimpleScanner(String(content));
    while (scanner.position < scanner.text.length) {
      let codePoint = scanner.readCodePoint();
      switch (codePoint) {
        case "<":
          result += "&lt;";
        break;
        case ">":
          result += "&gt;";
        break;
        case "&":
          result += "&amp;";
        break;
        case " ":
          result += "&nbsp;";
        break;
        case "\"":
          result += "&quot;";
        break;
        case "'":
          result += "&#x27;";
        break;
        default:
          result += codePoint;
        break;
      }
    }
    return result;
  }

  static injectUIElements(body) {
    const $ = BrowserUI.createMarkup;
    body.innerHTML = BrowserUI.concat(
      $('div', 'id="overview-pane" class="vertical-fill"', [
        $('div', 'class="header"', [
          $('span', 'class="title"', [ 'Test Sets' ]),
          $('span', 'class="failed label"'),
          $('span', 'class="passed label"')
        ]),
        $('div', 'class="pending separator"', [
          $('div', 'class="failed segment"')
        ]),
        $('div', 'class="beckoning contents"', [
          $('ul', 'id="test-list"'),
          $('div', 'class="load-area"', [
            $('button', 'id="file-source-chooser"', [ 'Open\u2026' ])
          ])
        ])
      ]),
      $('div', 'id="details-pane" class="vertical-fill"', [
        $('div', 'class="header"', [
          $('span', 'class="title"'),
          $('span', 'class="failed label"'),
          $('span', 'class="passed label"')
        ]),
        $('div', 'class="pending separator"', [
          $('div', 'class="failed segment"')
        ]),
        $('div', 'class="contents"')
      ])
    );
  }

  static applySkin(head) {
    let styleElement = BrowserUI.createChildElement(head, "style");
    let $ = styleElement.sheet.insertRule.bind(styleElement.sheet);
    $("body {" +
      " background-color: #141618;" +
      " color: rgba(255, 255, 255, 0.8);" +
      " font-family: sans-serif;" +
      " font-weight: 500;" +
      " margin: 0.5em;" +
    "}");

    $(".absent {" +
      " display: none;" +
    "}");

    $(".vertical-fill {" +
      /* NB: this factor must match container (body)'s margin above; also need
       * to include offset for top+bottom border--it's not sufficient merely
       * to set `box-sizing: border-box` for some reason... */
      " height: calc(100vh - 2 * 0.5em - 2 * 1px);" +
      " overflow-y: auto;" +
    "}");

    $("#overview-pane {" +
      " min-width: 32ch;" +
      " float: left;" +

      /* NB: top/bottom must match border width from .vertical-fill above */
      " border: 1px solid #101314;" +
      " background-color: #1c2022;" +
    "}");

    $("#details-pane {" +
      " background-color: #24282a;" +

      /* NB: top/bottom must match border width from .vertical-fill above */
      " border: 1px solid #101314;" +
      " border-left: 0;" +
      " border-right: 0;" +
    "}");

    $(".contents {" +
      " padding: 1em;" +
    "}");

    $(".header {" +
      " padding: 1em;" +
      " font-weight: 600;" +
      " font-stretch: semi-condensed;" +
      " color: rgba(255, 255, 255, 0.4);" +
    "}");

    $(".load-area {" +
      " text-align: center;" +
      " padding: 4em 0;" +
    "}");

    $("#file-source-chooser {" +
      " width: calc(100% - 2 * 1em - 2 * 1em);" +
      " padding: 0.5em 1em;" +
      " background-color: #161a1b;" +
      " border: 1px solid #161a1b;" +
      " border-radius: 2px;" +
      " color: rgba(255, 255, 255, 0.8);" +
    "}");

    $("#file-source-chooser:hover {" +
      " border: 1px solid #2b6287;" +
      " color: rgba(255, 255, 255, 1.0);" +
    "}");

    $("#overview-pane .contents:not(.beckoning) .load-area {" +
      " display: none;" +
    "}");

    $(".header .title {" +
      " margin-right: 1.5em;" +
    "}");

    $(".header .failed.label {" +
      " color: rgba(242, 119, 119, 0.8);" +
      " margin-left: 0.75em;" +
    "}");

    $(".header .passed.label {" +
      " color: rgba(93, 167, 0, 0.8);" +
      " margin-left: 0.75em;" +
    "}");

    $(".separator {" +
      " width: 100%;" +
    "}");

    $(".separator .failed.segment {" +
      " width: 0;" +
      " height: 2px;" +
    "}");

    $("#overview-pane .separator {" +
      " background-color: rgba(93, 167, 0, 0.8);" +
    "}");

    $("#overview-pane .pending.separator {" +
      " background-color: rgba(255, 255, 255, 0.4);" +
    "}");

    $("#overview-pane .separator .failed.segment {" +
      " background-color: rgba(242, 119, 119, 0.8);" +
    "}");

    $("#details-pane .separator {" +
      " background-color: rgba(93, 167, 0, 1.0);" +
      /* NB: this left/right margin should match header padding */
      " margin: 0 1em;" +
      /* NB: the factor here should match the left/right margin above */
      " width: calc(100% - 2 * 1em);" +
    "}");

    $("#details-pane .pending.separator {" +
      " background-color: rgba(255, 255, 255, 0.5);" +
    "}");

    $("#details-pane .separator .failed.segment {" +
      " background-color: rgba(242, 119, 119, 1.0);" +
    "}");

    $("#details-pane .header {" +
      " color: rgba(255, 255, 255, 0.5);" +
    "}");

    $("#details-pane .header .shortname {" +
      " color: rgba(255, 255, 255, 0.8);" +
      " font-weight: 600;" +
    "}");

    $("#details-pane .header .failed.label {" +
      " color: rgba(242, 119, 119, 1.0);" +
    "}");

    $("#details-pane .header .passed.label {" +
      " color: rgba(93, 167, 0, 1.0);" +
    "}");

    $("#overview-pane ul, #overview-pane .contents {" +
      " padding: 0;" +
    "}");

    $("#overview-pane .beckoning.contents #test-list {" +
      " display: none;" +
    "}");

    $(".test-set {" +
      " list-style-type: none;" +
      " line-height: 2.25em;" +
      " border-left: 2px solid transparent;" +
      " cursor: pointer;" +
      " color: rgba(255, 255, 255, 0.5);" +
    "}");

    $(".test-set:hover:not(.selected) {" +
      " border-left-color: #2b6287;" +
      " background-color: #161a1b;" +
    "}");

    $(".test-set.selected {" +
      " background-color: #141618;" +
      " border-left-color: #40a9f3;" +
    "}");

    $(".test-set:before {" +
      " margin-left: 1em;" +
      " content: '\\2739';" +
    "}");

    $(".failed.test-set {" +
      " color: rgba(242, 119, 119, 0.8);" +
    "}");

    $(".failed.test-set:before {" +
      " content: '\\2718';" +
    "}");

    $(".passed.test-set {" +
      " color: rgba(93, 167, 0, 0.8);" +
    "}");

    $(".passed.test-set:before {" +
      " content: '\\2714';" +
    "}");

    $(".test-set .label {" +
      " margin: 1em;" +
      " color: rgba(255, 255, 255, 0.8);" +
    "}");

    $(".test-detail {" +
      " background-color: #1c2022;" +
      " margin: 1em 0;" +
      " border-radius: 2px;" +
    "}");

    $(".test-detail .item-info {" +
      " line-height: 2em;" +
    "}");

    $(".test-detail .sigil {" +
      " background-color: #191c1d;" +
      " width: 2.25em;" +
      " display: inline-block;" +
      " text-align: center" +
    "}");

    $(".test-detail .label {" +
      " padding: 0 0.75em;" +
    "}");

    $(".failed.test-detail .sigil:after {" +
      " color: rgba(242, 119, 119, 1.0);" +
      " content: '\\2718';" +
    "}");

    $(".failed.test-detail .error-info {" +
      " color: rgba(255, 255, 255, 0.8);" +
      " background-color: rgba(0, 0, 0, 0.5);" +
      " font-family: monospace;" +
      " white-space: pre-wrap;" +
      " line-height: 2em;" +
      " padding: 1em;" +
      " overflow-x: auto;" +
    "}");

    $(".passed.test-detail .sigil:after {" +
      " color: rgba(93, 167, 0, 1.0);" +
      " content: '\\2714';" +
    "}");
  }
}

BrowserUI.CONTEXT_PSEUDOPATH = "/#!/execution-context";

BrowserUI.TextSplice = class TextSplice {
  constructor(parts = []) {
    this._parts = parts;
  }

  append(markup) {
    this._parts.push(markup);
  }

  toString() {
    return this._parts.join("");
  }
}

BrowserUI.UIState = class UIState {
  constructor(oldState = null, delta = null) {
    Object.assign(this, oldState, delta);
  }
}

BrowserUI.UIState.prototype.configProblem = null;
BrowserUI.UIState.prototype.newResults = null;
BrowserUI.UIState.prototype.testPaths = null;
BrowserUI.UIState.prototype.visibleDetails = null;
