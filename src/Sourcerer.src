import { TextSplicer } from "./TextSplicer.src";
import { TripleScanner } from "./TripleScanner.src";

export
class Sourcerer {
  constructor(text) {
    this._text = text;
  }

  static makeDataURI(text, contentType = "text/plain") {
    return "data:" + contentType + ";base64," + btoa(text);
  }

  static makeIIFE(source, argumentName = "", argumentValue = "") {
    let fn = new Function(argumentName, source);
    return "(" + fn.toString() + ")(" + argumentValue + ");\n";
  }

  static rewrite(text, imports, exports) {
    let result = new TextSplicer(text);
    let trailer = "";

    let $itransform = (importInfo) => {
      let { list, specifier } = importInfo;
      let result = "var { " + list.join(", ") + " } = " +
        "require(" + specifier + ");";
      let [ start, end ] = importInfo.span;
      let lineCount = text.substring(start, end).split(/\r\n|\n|\r/).length;
      while (lineCount > 1) {
        result += "\n";
        --lineCount;
      }
      return result;
    };

    for (let i = 0, n = imports.length; i < n; ++i) {
      let [ start, end ] = imports[i].span;
      result.splice(start, end, $itransform(imports[i]));
    }

    let $etransform = (exportInfo) => {
      let { name } = exportInfo;
      trailer += "\nexports." + name + " = " + name + ";"; // XXX do better
      return "/** @exports */";
    };

    for (let i = 0, n = exports.length; i < n; ++i) {
      let [ start ] = exports[i].span, end = exports[i].mark;
      result.splice(start, end, $etransform(exports[i]));
    }

    return String(result) + trailer;
  }

  extractClassDefinition(constructor) {
    let scanner = new TripleScanner(this._text);
    var current = scanner.getNextToken();
    while (current != null) {
      if (current.type == TripleScanner.DELIMITER_OPEN) {
        let source = "";
        current = scanner.getNextToken();
        // assert(current.type == TripleScanner.WHITESPACE)
        current = scanner.getNextToken();
        // assert(current.type == TripleScanner.TEXT)
        while (current.content.startsWith("///")) {
          while (scanner.tip == "\n") {
            scanner.readCodePoint();
          }
          current = scanner.getNextToken();
        }
        if (current.content != "class") {
          continue;
        }
        source += current.content;
        current = scanner.getNextToken();
        // assert(current.type == TripleScanner.WHITESPACE)
        source += current.content;
        current = scanner.getNextToken();
        if (current.content != constructor.name) {
          continue;
        }
        do {
          if (current == null) throw new Error("Unexpected EOF");
          source += current.content;
          current = scanner.getNextToken();
        } while (current.type != TripleScanner.DELIMITER_CLOSE);
        return source;
      }
      current = scanner.getNextToken();
    }
    throw new Error("Class not found: " + constructor.name);
  }
}
