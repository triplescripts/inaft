import { ExecutionTimeout } from "./ExecutionTimeout.src";
import { InaftPathResolver } from "./InaftPathResolver.src";
import { InaftTestHelper } from "./InaftTestHelper.src";
import { Loader } from "./Loader.src";

export
class ExecutionContext {
  constructor(parent, isMagic, loader = null) {
    this.parent = parent;
    this.isMagic = isMagic;
    this.loader = loader;
    this.pathResolver = new InaftPathResolver(isMagic);
    if (parent) {
      if (loader) throw new Error("Non-root execution context");
      this.exceptions = null;
    } else {
      this.exceptions = new Map();
    }
    this.timer = null;
  }

  static createRoot(system, withMagic = false, loader = new Loader()) {
    let result = new ExecutionContext(null, withMagic, loader);
    result.timer = new ExecutionTimeout();

    let helperModule = Loader.createModule(InaftPathResolver.HELPER_PATH);
    helperModule.exports = new InaftTestHelper(system, result.timer);
    loader.cacheModule(helperModule);

    return result;
  }

  static setUpTimer(context, $init) {
    let rootContext = ExecutionContext.getRoot(context);
    ++rootContext.timer.contention;
    rootContext.timer.start($init)
    return Promise.resolve();
  }

  static preemptTimerIfDone(context) {
    let rootContext = ExecutionContext.getRoot(context);
    --rootContext.timer.contention;
    if (!rootContext.timer.contention) {
      rootContext.timer.burn();
    }
    return rootContext.timer;
  }

  recordException(path, exception) {
    if (this.parent) throw new Error("Non-root execution context");
    let list = this.exceptions.get(path) || [];
    if (!list.length) {
      this.exceptions.set(path, list);
    }
    list.push(exception);
  }

  getTestResults() {
    if (!this.loader) throw new Error("Disassociated execution context");
    let help = this.loader.getCachedModule(InaftPathResolver.HELPER_PATH);
    return help.exports.getResults();
  }

  static evaluate(context, name, text) {
    var resolver = context.pathResolver;
    var rootContext = ExecutionContext.getRoot(context);
    try {
      rootContext.loader.load(name, text, resolver);
    } catch (ex) {
      rootContext.recordException(name, ex);
      return ex;
    }
    return null;
  }

  static getRoot(context) {
    while (context.parent) {
      context = context.parent;
    }
    return context;
  }
}
