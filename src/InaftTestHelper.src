import { InaftPathResolver } from "./InaftPathResolver.src";

export
class InaftTestHelper {
  constructor(readable, timer) {
    this._readable = readable;
    this._timer = timer;

    this.read = this.$read.bind(this);
    this.test = this.$test.bind(this);
    this.test.is = this.is = this.$is.bind(this);
    this.test.ok = this.ok = this.$ok.bind(this);

    this._blockers = [];

    this._currentID = 1;
    this._infos = [];
  }

  $read(path, wantRaw = false) {
    let realPath = InaftPathResolver.getRealNameForMagicPath(path);
    if (!realPath) {
      throw new Error("Illegal read for path " + JSON.stringify(path));
    }
    return this._readable.read(realPath, wantRaw);
  }

  $is(a, b) {
    if (a !== b) throw new Error(
      "expected " + b + " got " + a
    );
  }

  $ok(x) {
    if (!x) throw new Error(
      "expected true expression"
    );
  }

  $test(callback) {
    ++this._timer.contention;
    let label = callback.name;
    let info = new InaftTestHelper.TestInfo(this._currentID++, label);
    this._infos.push(info);
    let $checkSyncAndAsyncErrors = (() => {
      try {
        return Promise.resolve(callback());
      } catch (ex) {
        return Promise.reject(ex);
      }
    });

    this._blockers.push(
      new Promise((resolve) => {
        $checkSyncAndAsyncErrors().catch((err) => {
          info.failure = new InaftTestHelper.AbnormalExceptionError(err);
        }).then(() => { // XXX read as `finally` (not yet widely supported)
          info.stopped = true;
          --this._timer.contention;
          if (!this._timer.contention) {
            this._timer.burn();
          }
        });

        this._timer.then(() => {
          if (!info.stopped) {
            info.failure = new InaftTestHelper.TestTimeoutError();
            info.stopped = true;
          }
          resolve();
        });
      })
    );
  }

  getResults() {
    return Promise.all(this._blockers).then(() => {
      return new Promise((resolve) => {
        resolve({
          passed: !this._infos.filter((x) => !!x.failure).length,
          details: this._infos.map((x) => x.toJSONObject())
        });
      })
    });
  }
}

InaftTestHelper.TestInfo = class TestInfo {
  constructor(id, label) {
    this.id = id;
    this.label = label;
    this.failure = null;
    this.stopped = false;
  }

  toJSONObject() {
    let result = Object.assign({ }, this);
    if (this.failure) {
      result.failure = {
        message: this.failure.message,
        stack: this.failure.stack
      }
    }
    return result;
  }
}

InaftTestHelper.AbnormalExceptionError =
  class AbnormalExceptionError extends Error
{
  constructor(err) {
    super("Abnormal exception from rejected promise or weird error type");

    if (typeof(err) == "object" && err != null) {
      var { message, stack } = err;
    }

    if (typeof(message) != "string") {
      let $serialize = (err) => {
        if (err === "") return "\"\" [empty string]";
        if (!err) return String(err);
        return JSON.stringify(err) || "definitely weird";
      }
      this.stack = null;
      this.message += ": (" + $serialize(err) + ")";
      err = this;
    } else if (typeof(stack) != "string") {
      err = Object.assign({ stack: null }, err);
    }

    return err;
  }
}

InaftTestHelper.TestTimeoutError = class TestTimeoutError extends Error {
  constructor() {
    super("Inaft test timeout");
  }
}
