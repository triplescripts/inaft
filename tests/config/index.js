import { test } from "$TEST_HELPER"

import { fail, mock, run } from "$PROJECT/tests/support.js";

import { TestRunner } from "$PROJECT/src/TestRunner.src";

test(function autodiscovery_without_config() {
  // No config.json should result in autodiscovery.
  let system = mock({
    list: () => Promise.resolve([
      "skipped/"
    ]),
    read: (path) => Promise.resolve(null)
  });

  return new Promise((resolve, reject) => {
    let $is = fail(test.is, reject);
    run(system).then((info) => {
      $is(info.configIssue, null);
      $is(info.contexts.length, 0);
      $is(info.skipped.length, 1);
      $is(info.skipped[0], "tests/skipped/index.js");
      resolve();
    });
  });
})

test(function autodiscovery_no_explicit_tests() {
  // ... same thing for a config.json that doesn't include an explicit `tests`
  let system = mock({
    list: () => Promise.resolve([
      "also-skipped/"
    ]),
    read: (path) => {
      if (path == "tests/config.json") {
        return Promise.resolve(`
        {
          "___bar": "baz"
        }
        `);
      }
      return Promise.resolve(null);
    }
  });

  return new Promise((resolve, reject) => {
    let $is = fail(test.is, reject);
    run(system).then((info) => {
      $is(info.configIssue, null);
      $is(info.contexts.length, 0);
      $is(info.skipped.length, 1);
      $is(info.skipped[0], "tests/also-skipped/index.js");
      resolve();
    });
  });
})

test(function explicit_tests_avoids_autodiscovery() {
  // ... but one that does include `tests` should force us to disregard
  // anything else.
  let system = mock({
    list: () => Promise.resolve([
      "does-not-matter/",
      // XXX Notice how we don't include "fum/"; really this method shouldn't
      // even get called by the test runner, but we implement it, anyway.
    ]),
    read: (path) => {
      if (path == "tests/config.json") {
        return Promise.resolve(`
        {
          "tests": [
            "listed-in-config"
          ]
        }
        `);
      }
      return Promise.resolve(null);
    }
  });

  return new Promise((resolve, reject) => {
    let $is = fail(test.is, reject);
    run(system).then((info) => {
      $is(info.configIssue, null);
      $is(info.contexts.length, 0);
      $is(info.skipped.length, 1);
      $is(info.skipped[0], "tests/listed-in-config/index.js");
      resolve();
    });
  });
})

test(function parse_error() {
  let system = mock({
    read: (path) => {
      if (path == "tests/config.json") {
        return Promise.resolve(`
        {
          // Neither comments (like this one) nor single quoted strings are
          // valid JSON, so parsing should fail.
          'tests': [
            "fum"
          ]
        }
        `);
      }
      return Promise.reject();
    }
  });

  return new Promise((resolve, reject) => {
    let $is = fail(test.is, reject);
    let $ok = fail(test.ok, reject);
    run(system).then((info) => {
      $is(info.contexts.length, 0);
      $is(info.skipped.length, 0);
      $ok(info.configIssue.message.startsWith(TestRunner.PARSE_ISSUE));
      resolve();
    });
  });
})

test(function preload_error() {
  // And attempts to preload non-existent files should cause a similar
  // configuration error.
  let system = mock({
    read: (path) => {
      if (path == "tests/config.json") {
        return Promise.resolve(`
        {
          "tests": [
            "bar"
          ],
          "preload": [
            "baz.js"
          ]
        }
        `);
      }
      if (path == "baz.js") {
        return Promise.resolve(null);
      }
      return Promise.reject();
    }
  });

  return new Promise((resolve, reject) => {
    let $is = fail(test.is, reject);
    let $ok = fail(test.ok, reject);
    run(system).then((info) => {
      $is(info.contexts.length, 0);
      $is(info.skipped.length, 0);
      $ok(info.configIssue.message.startsWith(TestRunner.PRELOAD_ISSUE));
      resolve();
    });
  });
})
