import { test } from "$TEST_HELPER";

import { R4Request } from "$PROJECT/src/R4Request.src";

test(function simple_get() {
  let request = new R4Request("GET", "/id", "example.local");
  test.is(String(request),
             "GET /id HTTP/1.1" +
    "\r\n" + "Host: example.local" +
    "\r\n" +
    "\r\n"
  );
})

test(function parse_simple_get() {
  let request = R4Request.from(
             "GET /id HTTP/1.1" +
    "\r\n" + "Host: example.local" +
    "\r\n" +
    "\r\n"
  );

  test.is(request.method, "GET");
  test.is(request.resource, "/id");
  test.is(request.headers.get("Host"), "example.local");
})

test(function simple_post() {
  let request = new R4Request("POST", "/id", "example.local");
  request.content = "test-3850840913.local";
  test.is(String(request),
             "POST /id HTTP/1.1" +
    "\r\n" + "Host: example.local" +
    "\r\n" + "Content-Length: 21" +
    "\r\n" +
    "\r\n" + "test-3850840913.local"
  );
})

test(function parse_simple_post() {
  let request = R4Request.from(
             "POST /id HTTP/1.1" +
    "\r\n" + "Host: example.local" +
    "\r\n" + "Content-Length: 21" +
    "\r\n" +
    "\r\n" + "test-3850840913.local"
  );

  test.is(request.method, "POST");
  test.is(request.resource, "/id");
  test.is(request.headers.get("Host"), "example.local");
  test.is(request.headers.get("Content-Length"), "21");
  test.is(request.content, "test-3850840913.local");
})

test(function supported_methods() {
  let request = null;

  request = new R4Request("GET", "/id", "example.local");
  test.is(R4Request.from(String(request)).method, request.method);

  request = new R4Request("HEAD", "/id", "example.local");
  test.is(R4Request.from(String(request)).method, request.method);

  request = new R4Request("OPTIONS", "/id", "example.local");
  test.is(R4Request.from(String(request)).method, request.method);

  request = new R4Request("POST", "/id", "example.local");
  test.is(R4Request.from(String(request)).method, request.method);
})
