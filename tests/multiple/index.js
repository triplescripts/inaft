import { test } from "$TEST_HELPER"

import { fail, mock, review, run } from "$PROJECT/tests/support.js";

test(() => {
  // Test sets with more than one test file shouldn't be a problem.
  const text1 = `
    import { test } from "$TEST_HELPER";

    test(() => {
      if (("foo").toUpperCase() != "FOO") throw new Error;
    })
  `;

  const text2 = `
    import { test } from "$TEST_HELPER";

    test(() => {
      if (("BAR").toLowerCase() != "bar") throw new Error;
    })
  `;

  const name1 = "tests/first-test-file/index.js";
  const name2 = "tests/second-test-file/index.js";
  let system = mock({
    list: () => Promise.resolve([
      "first-test-file/",
      "second-test-file/"
    ]),
    read: (path) => {
      if (path == name1) return Promise.resolve(text1);
      if (path == name2) return Promise.resolve(text2);
      return Promise.resolve(null);
    }
  });

  return new Promise((resolve, reject) => {
    let $is = fail(test.is, reject);
    let $ok = fail(test.ok, reject);
    run(system).then((info) => {
      $is(info.contexts.length, 2);

      $is(info.paths[0], name1);
      $is(info.paths[1], name2);
      $is(info.contexts[0].exceptions.get(name1), undefined);
      $is(info.contexts[1].exceptions.get(name2), undefined);

      let awaiting1 =
      review(info.contexts[0]).then(({ exceptions, results }) => {
        $is(exceptions.length, 0);
        $ok(results.passed);
      });

      let awaiting2 =
      review(info.contexts[1]).then(({ exceptions, results }) => {
        $is(exceptions.length, 0);
        $ok(results.passed);
      });

      Promise.all([ awaiting1, awaiting2 ]).then(() => void(resolve()));
    });
  });
})
