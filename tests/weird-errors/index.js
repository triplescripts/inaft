import { test } from "$TEST_HELPER"

import { fail, mock, review, run } from "$PROJECT/tests/support.js";

import { InaftTestHelper } from "$PROJECT/src/InaftTestHelper.src";

const notice = "Abnormal exception from rejected promise or weird error type";

const $coerce = (err) => (new InaftTestHelper.AbnormalExceptionError(err));

test(function simple_coercion_falsy() {
  test.ok($coerce(false).message.startsWith(notice));
  test.ok($coerce(null).message.startsWith(notice));
  test.ok($coerce(undefined).message.startsWith(notice));
  test.ok($coerce("").message.startsWith(notice));
})

test(function simple_coercion_true() {
  test.ok($coerce(true).message.startsWith(notice));
})

test(function simple_coercion_object() {
  test.ok($coerce(Object.create(null)).message.startsWith(notice));
})

test(function simple_coercion_kindof_errors_but_not_really() {
  test.ok($coerce({ message: null }).message.startsWith(notice));
  test.ok($coerce({ message: { } }).message.startsWith(notice));
})

test(function throws_null() {
  // Suppose they throw a non-error.  Maybe not even an object.
  const text = `
    import { test } from "$TEST_HELPER";

    test(() => {
      throw null;
    })
  `;

  const name = "tests/null/index.js";
  let system = mock({
    list: () => Promise.resolve([
      "null/"
    ]),
    read: (path) => {
      if (path == name) return Promise.resolve(text);
      return Promise.resolve(null);
    }
  });

  return new Promise((resolve, reject) => {
    let $ok = fail(test.ok, reject);
    let $is = fail(test.is, reject);
    run(system).then((info) => {
      $is(info.contexts.length, 1);
      $is(info.paths[0], name);
      $is(info.contexts[0].exceptions.get(name), undefined);
      review(info.contexts[0]).then(({ results }) => {
        $ok(!results.passed);

        $is(results.details.length, 1);
        let { failure } = results.details[0];
        $ok(failure.message.startsWith(notice));

        resolve();
      });
    });
  });
})

test(function throws_string() {
  // Since JS even lets you throw strings.  Here's a hardcoded failure with
  // really wonky exception semantics:
  const text = `
    import { test } from "$TEST_HELPER";

    test(() => {
      throw "a thing happened";
    })
  `;

  const name = "tests/string/index.js";
  let system = mock({
    list: () => Promise.resolve([
      "string/"
    ]),
    read: (path) => {
      if (path == name) return Promise.resolve(text);
      return Promise.resolve(null);
    }
  });

  return new Promise((resolve, reject) => {
    let $ok = fail(test.ok, reject);
    let $is = fail(test.is, reject);
    run(system).then((info) => {
      $is(info.contexts.length, 1);
      $is(info.paths[0], name);
      $is(info.contexts[0].exceptions.get(name), undefined);
      review(info.contexts[0]).then(({ results }) => {
        $ok(!results.passed);

        $is(results.details.length, 1);
        let { failure } = results.details[0];
        $ok(failure.message.startsWith(notice));

        resolve();
      });
    });
  });
})

test(function reject_idiom() {
  const text = `
    import { test } from "$TEST_HELPER";

    test(() => {
      return Promise.reject();
    })
  `;

  const name = "tests/rejected/index.js";
  let system = mock({
    list: () => Promise.resolve([
      "rejected/"
    ]),
    read: (path) => {
      if (path == name) return Promise.resolve(text);
      return Promise.resolve(null);
    }
  });

  return new Promise((resolve, reject) => {
    let $ok = fail(test.ok, reject);
    let $is = fail(test.is, reject);
    run(system).then((info) => {
      $is(info.contexts.length, 1);
      $is(info.paths[0], name);
      $is(info.contexts[0].exceptions.get(name), undefined);
      review(info.contexts[0]).then(({ results }) => {
        $ok(!results.passed);

        $is(results.details.length, 1);
        let { failure } = results.details[0];
        $ok(failure.message.startsWith(notice));

        resolve();
      });
    });
  });
})

test(function reject_string() {
  const text = `
    import { test } from "$TEST_HELPER";

    test(() => {
      return Promise.reject("This is the reason for rejection");
    })
  `;

  const name = "tests/rejected-string/index.js";
  let system = mock({
    list: () => Promise.resolve([
      "rejected-string/"
    ]),
    read: (path) => {
      if (path == name) return Promise.resolve(text);
      return Promise.resolve(null);
    }
  });

  return new Promise((resolve, reject) => {
    let $ok = fail(test.ok, reject);
    let $is = fail(test.is, reject);
    run(system).then((info) => {
      $is(info.contexts.length, 1);
      $is(info.paths[0], name);
      $is(info.contexts[0].exceptions.get(name), undefined);
      review(info.contexts[0]).then(({ results }) => {
        $ok(!results.passed);

        $is(results.details.length, 1);
        let { failure } = results.details[0];
        $ok(failure.message.startsWith(notice));

        resolve();
      });
    });
  });
})

test(function throws_kindof_error_but_not_really() {
  const text = `
    import { test } from "$TEST_HELPER";

    test(() => {
      throw {
        message: { toString: () => "so close, but still not what we expect" }
      };
    })
  `;

  const name = "tests/alien-object/index.js";
  let system = mock({
    list: () => Promise.resolve([
      "alien-object/"
    ]),
    read: (path) => {
      if (path == name) return Promise.resolve(text);
      return Promise.resolve(null);
    }
  });

  return new Promise((resolve, reject) => {
    let $ok = fail(test.ok, reject);
    let $is = fail(test.is, reject);
    run(system).then((info) => {
      $is(info.contexts.length, 1);
      $is(info.paths[0], name);
      $is(info.contexts[0].exceptions.get(name), undefined);
      review(info.contexts[0]).then(({ results }) => {
        $ok(!results.passed);

        $is(results.details.length, 1);
        let { failure } = results.details[0];
        $ok(failure.message.startsWith(notice));

        resolve();
      });
    });
  });
})

test(function reject_custom_error() {
  const text = `
    import { test } from "$TEST_HELPER";

    test(() => {
      let $createError = (output) => ({ message: output });
      return Promise.reject($createError("this is a custom error object"));
    })
  `;

  const name = "tests/custom-error/index.js";
  let system = mock({
    list: () => Promise.resolve([
      "custom-error/"
    ]),
    read: (path) => {
      if (path == name) return Promise.resolve(text);
      return Promise.resolve(null);
    }
  });

  return new Promise((resolve, reject) => {
    let $ok = fail(test.ok, reject);
    let $is = fail(test.is, reject);
    run(system).then((info) => {
      $is(info.contexts.length, 1);
      $is(info.paths[0], name);
      $is(info.contexts[0].exceptions.get(name), undefined);
      review(info.contexts[0]).then(({ results }) => {
        $ok(!results.passed);

        $is(results.details.length, 1);
        let { failure } = results.details[0];
        $is(failure.message, "this is a custom error object");

        resolve();
      });
    });
  });
})
