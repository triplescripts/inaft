import { test } from "$TEST_HELPER";

import { FileCache } from "$PROJECT/src/FileCache.src";
import { FileUtils } from "$PROJECT/src/FileUtils.src";

test(function list_without_common_prefix_bug1() {
  let cache = new FileCache();
  cache.store("this is in foo/", "foo/index.txt")
  cache.store("this is, too", "foo/index2.txt")
  cache.store("this isn't in foo/", "foobar/index.txt")

  let entries = FileUtils.list(cache, "foo");
  test.is(entries.length, 2);
  test.ok(entries.indexOf("index.txt") >= 0);
  test.ok(entries.indexOf("index2.txt") >= 0);
})

test(function list_without_common_prefix_bug2() {
  let cache = new FileCache();
  cache.store("this is in foo/", "foo/index.txt")
  cache.store("this is, too", "foo/index2.txt")
  cache.store("this isn't in foo/", "foobar/index.txt")

  let entries = FileUtils.list(cache, "foo/");
  test.is(entries.length, 2);
  test.ok(entries.indexOf("index.txt") >= 0);
  test.ok(entries.indexOf("index2.txt") >= 0);
})
