import { test } from "$TEST_HELPER"

import { fail, mock, run } from "$PROJECT/tests/support.js";

import { InaftTestHelper } from "$PROJECT/src/InaftTestHelper.src";

const timeout = (context) => void(context.timer.burn());

test(() => {
  // It's possible that a test itself is buggy and hands us a promise that
  // never resolves.  Inaft has a timeout-based approach for automatically
  // marking tests as failures if they haven't completed within some time
  // budget.  Let's simulate what happens when the timer burns down for a
  // Promise-based test that hasn't resulvode.
  const text = `
    import { test } from "$TEST_HELPER"

    test(() => {
      // Guaranteed not to resolve.
      return new Promise(() => void(0));
    })
  `;

  const name = "tests/unsettled/index.js";
  let system = mock({
    list: () => Promise.resolve([
      "unsettled/"
    ]),
    read: (path) => {
      if (path == name) return Promise.resolve(text);
      return Promise.resolve(null);
    }
  });

  return new Promise((resolve, reject) => {
    let $is = fail(test.is, reject);
    let $ok = fail(test.ok, reject);
    run(system).then((info) => {
      $is(info.contexts.length, 1);
      $is(info.paths[0], name);
      $is(info.contexts[0].exceptions.get(name), undefined);

      timeout(info.contexts[0]);
      info.contexts[0].getTestResults().then((results) => {
        $ok(!results.passed);

        const { message } = new InaftTestHelper.TestTimeoutError();

        $is(results.details.length, 1);
        let { failure } = results.details[0];
        $is(failure.message, message);

        resolve();
      });
    });
  });
})
