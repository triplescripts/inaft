import { test } from '$TEST_HELPER';

import { DependencySniffer } from '$PROJECT/src/DependencySniffer.src';

test(() => {
  var parser, text = "";
  text += "import { foo } from 'bar.js';" + "\n";
  parser = new DependencySniffer(text);
  var imports = parser.getImports();
  test.is(imports.length, 1);
  test.is(String(imports[0].list), "foo");
  test.is(imports[0].specifier, "'bar.js'");
  let [ start, end ] = imports[0].span;
  test.is(start, 0);
  test.is(end, text.indexOf("\n"));
});

test(() => {
  var parser, text = "";
  text += "import { test } from '$TEST_HELPER';" + "\n";
  text += "import { foo } from 'bar.js';" + "\n";
  parser = new DependencySniffer(text);
  var imports = parser.getImports();
  test.is(imports.length, 2);
  test.is(String(imports[0].list), "test");
  test.is(imports[0].specifier, "'$TEST_HELPER'");
  let [ start, end ] = imports[0].span;
  test.is(start, 0);
  test.is(end, text.indexOf("\n"));
});

test(() => {
  var parser, text = "";
  text += "import { foo } from 'bar.js';" + "\n";
  text += "function baz() { return 1; }" + "\n";
  text += "import { test } from '$TEST_HELPER';" + "\n";
  parser = new DependencySniffer(text);
  var imports = parser.getImports();
  test.is(imports.length, 2);
  test.is(String(imports[0].specifier), "'bar.js'");
  test.is(String(imports[1].specifier), "'$TEST_HELPER'");
});

test(() => {
  var parser, text = "";
  text += "import { foo, bar } from 'baz.js';" + "\n";
  parser = new DependencySniffer(text);
  var imports = parser.getImports();
  test.is(imports.length, 1);
  test.is(String(imports[0].specifier), "'baz.js'");
  test.is(imports[0].list.length, 2);
  test.is(String(imports[0].list), "foo,bar");
});

test(() => {
  var parser, text = "";
  text += "// import { foo } from 'bar.js';" + "\n";
  text += " * import { foo } from 'bar.js';" + "\n";
  text += "} import { foo } from 'bar.js';" + "\n";
  parser = new DependencySniffer(text);
  var imports = parser.getImports();
  test.is(imports.length, 0);
});

test(() => {
  var parser, text = "";
  // Check ASI support
  text += "import { foo } from 'bar.js'" + "\n";
  text += "\n";
  text += "const baz = 0;" + "\n";
  parser = new DependencySniffer(text);
  var imports = parser.getImports();
  test.is(imports.length, 1);
  test.is(String(imports[0].specifier), "'bar.js'");
  test.is(String(imports[0].list), "foo");
});

test(() => {
  var parser, text = "";
  text += "export class foo;" + "\n";
  parser = new DependencySniffer(text);
  var exports = parser.getExports();
  test.is(exports.length, 1);
  test.is(exports[0].kind, "class");
  test.is(exports[0].name, "foo");
});

test(() => {
  var parser, text = "";
  text += "export function bar() { }" + "\n";
  parser = new DependencySniffer(text);
  var exports = parser.getExports();
  test.is(exports.length, 1);
  test.is(exports[0].kind, "function");
  test.is(exports[0].name, "bar");
});

test(() => {
  var parser, text = "";
  text += "export const baz;" + "\n";
  parser = new DependencySniffer(text);
  var exports = parser.getExports();
  test.is(exports.length, 1);
  test.is(exports[0].kind, "const");
  test.is(exports[0].name, "baz");
});

test(() => {
  var parser, text = "";
  text += "export let fum;" + "\n";
  parser = new DependencySniffer(text);
  var exports = parser.getExports();
  test.is(exports.length, 1);
  test.is(exports[0].kind, "let");
  test.is(exports[0].name, "fum");
});

test(() => {
  var parser, text = "";
  text += "export var nom;" + "\n";
  parser = new DependencySniffer(text);
  var exports = parser.getExports();
  test.is(exports.length, 1);
  test.is(exports[0].kind, "var");
  test.is(exports[0].name, "nom");
});
