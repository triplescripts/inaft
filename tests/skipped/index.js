import { test } from "$TEST_HELPER"

import { fail, mock, run } from "$PROJECT/tests/support.js";

test(() => {
  // When a test has no index.js, it should be skipped.
  let system = mock({
    read: (path) => Promise.resolve(null),
    list: () => Promise.resolve([
      "skipped/"
    ])
  });

  return new Promise((resolve, reject) => {
    let $is = fail(test.is, reject);
    run(system).then((info) => {
      $is(info.contexts.length, 0);
      $is(info.skipped.length, 1);
      $is(info.skipped[0], "tests/skipped/index.js");
      resolve();
    });
  });
})
