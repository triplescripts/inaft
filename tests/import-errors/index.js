import { test } from "$TEST_HELPER"

import { fail, mock, release, review, run } from "$PROJECT/tests/support.js";

test(function direct_import_failure() {
  // We expect this to cause a top-level exception for our test file, due to
  // trying to import a file that can't be imported, since it doesn't exist.
  const text =
    `import { foo } from "$PROJECT/foo.js";

    foo.check();
  `;

  const name = "tests/broken/index.js";
  let system = mock({
    list: () => Promise.resolve([
      "broken/"
    ]),
    read: (path) => {
      if (path == name) return Promise.resolve(text);
      return Promise.resolve(null); // including foo.js
    }
  });

  return new Promise((resolve, reject) => {
    let $is = fail(test.is, reject);
    let $ok = fail(test.ok, reject);
    run(system).then((info) => {
      $is(info.contexts.length, 1);
      $is(info.paths[0], name);

      const notice = 'Cannot resolve module "$PROJECT/foo.js"'; // + ...

      let exceptions = info.contexts[0].exceptions.get(name);
      $is(exceptions.length, 1);
      $ok(exceptions[0].message.startsWith(notice));

      release(info.contexts[0]).then(() => resolve());
    });
  });
})

test(function transitive_import_failure() {
  // And now something slightly different...
  const text =
    `import { foo } from "$PROJECT/foo.js";

    foo.check();
  `;

  // Suppose foo.js does exist, but *it* tries to import some other module
  // that doesn't.  We expect this error to be reported to us just the same.
  const fooText =
    `import { bar } from "bar.js";

    `+`export
    class foo {
      static check() {
        let isOkay = bar.init('baz');
        if (!isOkay) throw new Error;
      }
    }
  `;

  const name = "tests/still-broken/index.js";
  let system = mock({
    list: () => Promise.resolve([
      "still-broken/"
    ]),
    read: (path) => {
      if (path == name) return Promise.resolve(text);
      if (path == "foo.js") return Promise.resolve(fooText);
      return Promise.resolve(null); // NB: no bar.js
    }
  });

  return new Promise((resolve, reject) => {
    let $is = fail(test.is, reject);
    let $ok = fail(test.ok, reject);
    run(system).then((info) => {
      $is(info.contexts.length, 1);
      $is(info.paths[0], name);

      review(info.contexts[0]).then(({ exceptions }) => {
        $is(exceptions.length, 2);

        const notice = 'Cannot resolve module "'; // + ...
        $ok(exceptions.pop().message.startsWith(notice + "$PROJECT/foo.js"));
        $ok(exceptions.pop().message.startsWith(notice + "bar.js"));

        resolve();
      });
    });
  });
})
