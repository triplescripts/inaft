import { test } from "$TEST_HELPER"

import { fail, mock, release, review, run } from "$PROJECT/tests/support.js";

test(function passing_no_helper() {
  // Let's demonstrate a simple test that should pass.  No use of `test` from
  // the test helper--just a simple conditional that would throw in the event
  // of an error.  Here's what that kind of test might look like:
  const text = `
    if (+0 !== -0) throw new Error("didn't expect signed zero");
  `;

  // And we need something that satisfies the system interface expected by the
  // test runner that would let it read this file.
  const name = "tests/foo/index.js";
  let system = mock({
    list: () => Promise.resolve([
      "foo/"
    ]),
    read: (path) => {
      if (path == name) return Promise.resolve(text);
      return Promise.resolve(null);
    }
  });

  // What should happen is that the when the testing is over, there are no
  // exceptions recorded for the test's execution context.  Need to use a
  // promise here to keep our own execution "open", since the test runner's
  // main logic is asynchronous.
  return new Promise((resolve, reject) => {
    let $is = fail(test.is, reject);
    run(system).then((info) => {
      $is(info.contexts.length, 1);
      $is(info.paths[0], name);
      $is(info.contexts[0].exceptions.get(name), undefined);
      release(info.contexts[0]).then(() => resolve());
    });
  });
})

test(function throws_no_helper() {
  // Okay, now let's check that simple tests with failures are handled
  // correctly.  Once again, no use of the test helper here.  In this case,
  // the test WILL cause an error to be thrown, since `1 + undefined` is NaN,
  // but `NaN === NaN` is never true, meaning there's a bug in the test
  // itself.
  const text = `
    if (1 + undefined !== NaN) throw new Error("expected NaN")
  `;

  const name = "tests/foo/index.js";
  let system = mock({
    list: () => Promise.resolve([
      "foo/"
    ]),
    read: (path) => {
      if (path == name) return Promise.resolve(text);
      return Promise.resolve(null);
    }
  });

  // Exceptions thrown when test files are evaluated get recorded.
  return new Promise((resolve, reject) => {
    let $is = fail(test.is, reject);
    run(system).then((info) => {
      $is(info.contexts.length, 1);
      $is(info.paths[0], name);

      let exceptions = info.contexts[0].exceptions.get(name);
      $is(exceptions.length, 1);
      $is(exceptions[0].message, "expected NaN");

      release(info.contexts[0]).then(() => resolve());
    });
  });
})

test(function passing_using_helper() {
  // Now let's make sure tests that use the test helper work.  No fancy stuff
  // here.  Just a test that's more or less hardcoded to pass.
  const text = `
    import { test } from "$TEST_HELPER";

    test(() => {
      test.ok(true);
    })
  `;

  const name = "tests/foo/index.js";
  let system = mock({
    list: () => Promise.resolve([
      "foo/"
    ]),
    read: (path) => {
      if (path == name) return Promise.resolve(text);
      return Promise.resolve(null);
    }
  });

  // Because this test uses the helper, we have actual test-aware records.
  return new Promise((resolve, reject) => {
    let $is = fail(test.is, reject);
    let $ok = fail(test.ok, reject);
    run(system).then((info) => {
      $is(info.contexts.length, 1);
      $is(info.paths[0], name);
      $is(info.contexts[0].exceptions.get(name), undefined);
      review(info.contexts[0]).then(({ results }) => {
        $ok(results.passed);
        resolve();
      });
    });
  });
})

test(function throws_using_helper() {
  // Now how about one that doesn't pass?
  const text = `
    import { test } from "$TEST_HELPER";

    test(() => {
      throw new Error("this was designed to fail");
    })
  `;

  const name = "tests/foo/index.js";
  let system = mock({
    list: () => Promise.resolve([
      "foo/"
    ]),
    read: (path) => {
      if (path == name) return Promise.resolve(text);
      return Promise.resolve(null);
    }
  });

  // And it won't be recorded as an exception in the execution context--that's
  // essentially just for eval errors or simple tests that don't use the test
  // helper.  In our case, we're able to check the helper's records for the
  // failure.
  return new Promise((resolve, reject) => {
    let $is = fail(test.is, reject);
    let $ok = fail(test.ok, reject);
    run(system).then((info) => {
      $is(info.contexts.length, 1);
      $is(info.paths[0], name);
      $is(info.contexts[0].exceptions.get(name), undefined);
      review(info.contexts[0]).then(({ results }) => {
        $ok(!results.passed);

        $is(results.details.length, 1);
        let { failure } = results.details[0];
        $is(failure.message, "this was designed to fail");

        resolve();
      });
    });
  });
})

test(function multiple_using_helper() {
  // And test files with multiple uses of the test helper need to work, too.
  const text = `
    import { test } from "$TEST_HELPER";

    test(() => {
      if (("foo").toUpperCase() != "FOO") throw new Error;
    })

    test(() => {
      if (("BAR").toLowerCase() != "bar") throw new Error;
    })
  `;

  const name = "tests/multiple-helper-calls/index.js";
  let system = mock({
    list: () => Promise.resolve([
      "multiple-helper-calls/"
    ]),
    read: (path) => {
      if (path == name) return Promise.resolve(text);
      return Promise.resolve(null);
    }
  });

  return new Promise((resolve, reject) => {
    let $is = fail(test.is, reject);
    let $ok = fail(test.ok, reject);
    run(system).then((info) => {
      $is(info.contexts.length, 1);
      $is(info.paths[0], name);
      $is(info.contexts[0].exceptions.get(name), undefined);
      review(info.contexts[0]).then(({ exceptions, results }) => {
        $ok(results.passed);
        $ok(results.details.length, 2);
        $is(exceptions.length, 0);
        resolve();
      });
    });
  });
})
