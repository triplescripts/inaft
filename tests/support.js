import { ExecutionContext } from "../src/ExecutionContext.src";
import { TestRunner } from "../src/TestRunner.src";

// NB; Try not to use `getTestResults` directly; use `review` (defined below)
// instead.  If the test uses `getTestResults`, you'll need to make sure to
// `release` contention for the mocked context, because otherwise the call to
// `getTestResults` causes it to block, awaiting burndown from the timer.

export
function mock(extras = { }) {
  let system = { };

  system.createContext = function (parent, wantMagic) {
    if (parent) {
      var context = new ExecutionContext(parent, wantMagic);
    } else {
      var context = ExecutionContext.createRoot(null, wantMagic);
    }
    return Promise.resolve(context);
  }

  system.resolvePath = function (context, spec, relativeTo) {
    return context.pathResolver.handle(spec, relativeTo);
  }

  system.loadScript = function (context, name, text) {
    return Promise.resolve(
      void(ExecutionContext.evaluate(context, name, text))
    );
  }

  system.startCountdown = function (context, millis) {
    return ExecutionContext.setUpTimer(context, () => void(0));
  }

  return Object.assign(system, extras);
}

export
function fail(check, reject) {
  return (...args) => {
    try {
      check.apply(null, args);
    } catch (ex) {
      reject(ex);
    }
  }
}

export
function run(system) {
  let harness = new TestRunner(system);
  let info = { contexts: [], paths: [], skipped: [], configIssue: null };
  return new Promise((resolve) => {
    harness.onfinish = () => void(resolve(info));
    harness.ontestskip = (path) => void(info.skipped.push(path));
    harness.onbadconfig = (err) => void(info.configIssue = err);
    harness.ontestexecute = (context, path) => {
      info.contexts.push(context);
      info.paths.push(path);
    };
    harness.runTests();
  });
}

export
function review(context) {
  let data = { exceptions: null, results: null };
  return release(context).then(() => (
    context.getTestResults().then((results) => {
      let lol = Array.from(context.exceptions.values());
      data.exceptions = lol.reduce((all, some) => all.concat(some), []);
      data.results = results;
      return Promise.resolve(data);
    })
  ));
}

export
function release(context) {
  return ExecutionContext.preemptTimerIfDone(context);
}
