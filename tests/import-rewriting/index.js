var { test } = require("$TEST_HELPER");

import { DependencySniffer } from "$PROJECT/src/DependencySniffer.src";
import { Sourcerer } from "$PROJECT/src/Sourcerer.src";

test(() => {
  var parser, text = "";
  text += "import { foo } from 'bar.js';" + "\n";
  parser = new DependencySniffer(text);
  test.is(Sourcerer.rewrite(text, parser.getImports(), []),
    "var { foo } = require('bar.js');" + "\n"
  );
});

test(() => {
  var parser, text = "";
  text += "import { test } from '$TEST_HELPER';" + "\n";
  text += "import { foo } from 'bar.js';" + "\n";
  parser = new DependencySniffer(text);
  test.is(Sourcerer.rewrite(text, parser.getImports(), []),
    "var { test } = require('$TEST_HELPER');" + "\n" +
    "var { foo } = require('bar.js');" + "\n"
  );
});

test(() => {
  var parser, text = "";
  text += "import { foo } from 'bar.js';" + "\n";
  text += "function baz() { return 1; }" + "\n";
  text += "import { test } from '$TEST_HELPER';" + "\n";
  parser = new DependencySniffer(text);
  test.is(Sourcerer.rewrite(text, parser.getImports(), []),
    "var { foo } = require('bar.js');" + "\n" +
    "function baz() { return 1; }" + "\n" +
    "var { test } = require('$TEST_HELPER');" + "\n"
  );
});

test(() => {
  var parser, text = "";
  text += "import { foo, bar } from 'baz.js';" + "\n";
  parser = new DependencySniffer(text);
  test.is(Sourcerer.rewrite(text, parser.getImports(), []),
    "var { foo, bar } = require('baz.js');" + "\n"
  );
});

test(() => {
  var parser, text = "";
  text += "// import { foo } from 'bar.js';" + "\n";
  text += " * import { foo } from 'bar.js';" + "\n";
  text += "} import { foo } from 'bar.js';" + "\n";
  parser = new DependencySniffer(text);
  test.is(Sourcerer.rewrite(text, parser.getImports(), []), text);
});

test(() => {
  var parser, text = "";
  text += "import {" + "\n";
  text += "  read," + "\n";
  text += "  test" + "\n";
  text += "} from '$TEST_HELPER';" + "\n";
  text += "function foo() { return -1; }" + "\n";
  text += "import { main } from '$PROJECT/index.js';" + "\n";
  parser = new DependencySniffer(text);
  test.is(Sourcerer.rewrite(text, parser.getImports(), []),
    "var { read, test } = require('$TEST_HELPER');" + "\n" +
    "\n" +
    "\n" +
    "\n" +
    "function foo() { return -1; }" + "\n" +
    "var { main } = require('$PROJECT/index.js');" + "\n"
  );
});
