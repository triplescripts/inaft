import { foo } from "autoload.js";

export
class Foo {
  static get foo() {
    return foo;
  }

  static get baz() {
    return "baz";
  }
}
