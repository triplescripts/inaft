var { test } = require('$TEST_HELPER');

var m = require('$PROJECT/relative-paths/index.js');

test(() => {
  test.is(m.foo, "BAR");
  test.is(m.fum, "baz");
});
