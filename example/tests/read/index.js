var { read, test } = require('$TEST_HELPER');

test(() => {
  return read("$PROJECT/tests/read/foo.txt").then((contents) => {
    test.is(contents, "bar\n");
  });
});
