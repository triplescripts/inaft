var { test } = require('$TEST_HELPER');

function halve(a) {
  return a / 2;
}

test.ok(halve(4) < 4);

test.ok(halve(2) < 2);

// This shows up in the UI
// as a meta-level failure
test.ok(halve(0) < 0);
