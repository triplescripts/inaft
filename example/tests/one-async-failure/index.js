var { read, test } = require('$TEST_HELPER');

test(() => {
  return read("$PROJECT/tests/one-async-failure/index.js").then((contents) => {
    test.is("bar", "baz");
  });
});
