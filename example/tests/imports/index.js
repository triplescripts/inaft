var { test } = require("$TEST_HELPER");

var { Foo } = require("$PROJECT/importer.js");

test(() => {
  test.is(Foo.foo, "BAR");
  test.is(Foo.baz, "baz");
});

m = require("$PROJECT/preload-importer.js");

test(() => {
  test.is(m.foo, "bar baz");
});

import { foo } from "$PROJECT/shared.js";

test(() => {
  test.is(foo, "bar");
});
