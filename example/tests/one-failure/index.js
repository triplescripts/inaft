var { test } = require('$TEST_HELPER');

// fails because "f" !== "F"
test(() =>
  test.is((15).toString(16), "F")
)
