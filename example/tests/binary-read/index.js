var { read, test } = require("$TEST_HELPER");

test(() => {
  return read("$PROJECT/tests/binary-read/example.bin", true).then((data) => {
    test.is(data.byteLength, 8);
    test.is(data[0], 0x05);
    test.is(data[1], 0x8d);
    test.is(data[2], 0x54);
    test.is(data[3], 0xd8);
    test.is(data[4], 0x0d);
    test.is(data[5], 0xf2);
    test.is(data[6], 0x1b);
    test.is(data[7], 0xf7);
  });
});
