var { test } = require('$TEST_HELPER');

var { baz } = require('$PROJECT/shorthand');

test(() => {
  test.is(baz, 'BAR');
});
