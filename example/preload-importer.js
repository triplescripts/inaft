/**
 * @fileoverview
 *
 * The import below is already listed in the preload section of config.json.
 * This demonstrates that our logic for handling static imports doesn't
 * conflict with the modules (scripts) already listed as preloads.
 */

import { foo } from "shared.js";

exports.foo = foo.toLowerCase() + " baz";
