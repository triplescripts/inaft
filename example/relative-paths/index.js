var { foo } = require('../shared.js');
var { fum } = require('./foo.js');

class RelativePaths {
  static get foo() {
    return foo.toUpperCase();
  }

  static get fum() {
    return fum;
  }
}

if (typeof(module) != "undefined") module.exports = RelativePaths;
