// This file should NOT be included in the preload section of the config.json.
import { bar } from "recursive-autoload.js";

module.exports.foo = bar.toUpperCase();
