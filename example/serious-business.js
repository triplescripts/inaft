var { foo } = require('shared.js');

function SeriousBusiness() {
  this.x = foo;
}

{
  let $proto = SeriousBusiness.prototype;

  $proto.foo = function() {
    return this.x;
  }
}

if (typeof(module) != "undefined") module.exports = SeriousBusiness;
