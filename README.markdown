Simple JS test runner for simple JS projects.

Inaft doesn't rely on any dependencies or third-party package registry, nor
does it need complex configuration or otherwise rely on familiarity with any
given toolchain.  Inaft is designed to be dropped directly into your project's
source repo and still be able to run your tests 20 years from now.

Test conventions
================

With Inaft, adding tests to your project works like this:

1. Copy Inaft's test runner into your `tests/` (e.g., `tests/harness.app.htm`)
2. Write your test (e.g., `tests/foo/index.js`)
3. There is no step 3; you're ready to run your tests

No package.json, no devDependencies, no configuration needed.

For tests, Inaft chooses (more or less) plain ol' ES6 modules as its preferred
format.  A module is a test suite, and exceptions are test failures.

It even runs in the browser.

This all makes Inaft especially well-suited for JS projects that are not part
of the NodeJS/NPM ecosystem.  Having said that, the Inaft test runner is a
polyglot file, so you *can* run it using NodeJS from the terminal, if that's
what you prefer.

Building
========

Inaft is built with Build.app.htm (included in the repo root dir).  Use it to
run the following command:

    build main.src -o harness.app.htm

Contributing
============

Add your name to ACKNOWLEDGEMENTS.txt, and use Build.app.htm to generate a new
credits.src with the following command:

    create-credits ./ACKNOWLEDGEMENTS.txt -o ./src/credits.src

License
=======

This work is made available under the terms of the MIT License.

Copyright individual authors and contributors; see ACKNOWLEDGEMENTS.txt.
